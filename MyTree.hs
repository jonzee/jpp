module MyTree (Tree, initTree, insert, getElement, treeElems) where

data Tree e = Empty | Node (Tree e) Int e (Tree e)

instance Eq (Tree e) where
  Empty == Empty = True
  Node l1 i1 e1 r1 == Node l2 i2 e2 r2 = l1 == l2 && i1 == i2 && r1 == r2
  _ == _ = False

instance Show a => Show (Tree a) where
  show t = case t of
    Empty -> "E"
    Node left i e right -> "N (i: " ++ show i ++ " e: " ++ show e ++ ")\n\n(" ++ show left ++ ")\n\n(" ++ show right ++ ")"

initTree :: Tree (Maybe e)
initTree = Empty

insert :: Tree (Maybe e) -> Int -> Int -> Int -> e -> Tree (Maybe e)
insert Empty ind size key el
  | key == (ind+half) = Node Empty key (Just el) Empty
  | key < (ind+half) = Node (insert Empty ind half key el) (ind+half) Nothing Empty
  | key > (ind+half) = Node Empty (ind+half) Nothing (insert Empty (half+1+ind) (size-half-1) key el)
  where half = quot size 2
insert (Node left i e right) ind size key el
  | key == i = Node left i (Just el) right
  | key < i = Node (insert left ind half key el) i e right
  | key > i = Node left i e (insert right (half+1+ind) (size-half-1) key el)
  where half = quot size 2

getElement :: Tree (Maybe e) -> Int -> e
getElement Empty ind = error ("MyTree (getElement): not initialized for: " ++ (show ind))
getElement (Node left i e right) ind
  | i > ind = getElement left ind
  | i < ind = getElement right ind
  | i == ind = fromJust e

treeElems :: Tree (Maybe e) -> [e]
treeElems Empty = []
treeElems (Node left i Nothing right) = treeElems left ++ treeElems right
treeElems (Node left i e right) = treeElems left ++ ((fromJust e) : treeElems right)

fromJust :: Maybe a -> a
fromJust Nothing = error "MyTree (fromJust): nothing"
fromJust (Just a) = a
