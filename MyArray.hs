module MyArray(Array, listArray, (!), elems, array, update, (//)) where
import MyTree
import Ix

data Array i e = Array (Tree (Maybe e)) (i, i) deriving (Show)

(!) :: Ix i => Array i e -> i -> e
(!) (Array tree rang) ind = getElement tree (index rang ind)

elems :: Ix i => Array i e -> [e] 
elems (Array tree rang) = treeElems tree

update :: Ix i => i -> e -> Array i e -> Array i e
update i e (Array tree rang) =
  Array (insert tree 0 (rangeSize rang) (index rang i) e) $ rang

(//) :: Ix i => Array i e -> [(i,e)] -> Array i e
(//) arr list = foldr (\(i,e) a -> update i e a) arr list

array :: Ix i => (i, i) -> [(i,e)] -> Array i e
array rang list = (//) (Array initTree rang) list

listArray :: Ix i => (i, i) -> [e] -> Array i e
listArray rang list = array rang (zip (range rang) list)
