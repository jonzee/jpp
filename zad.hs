{-
TO DO:
- dopisać konstruktor innej nazwy (Array i e -> Ar ...)
- przemyśleć drzewo!!!
- zmienić rang na size w Array (bo właściwie tylko do tego sluży)
-}


-- Napisz moduł MyArray, eksportujący:
-- klasę indeksów (w razie potrzeby można dodać więcej metod):

-- KLASA INDEKSÓW!!!
class Ord a => Ix a where
  range :: (a, a) -> [a]
  index :: (a, a) -> a -> Int
  inRange :: (a, a) -> a -> Bool
  rangeSize :: (a, a) -> Int

toTuple :: [a] -> [b] -> [(a,b)]
toTuple [] _ = []
toTuple (x:xs) l = (map ((,) x) l) ++ (toTuple xs l)

index' :: Ix a => [a] -> a -> Int
index' [] _ = 0
index' (x:xs) a =
  if (x == a) then 0
  else (+1) (index' xs a)

index'' :: Ix a => (a, a) -> a -> Int
index'' (h, t) a =
  if (inRange (h, t) a) then
    index' (range (h, t)) a
  else
    error "MyArray: index out of range"

instance Ix Int where
  range (h, t) = [h..t]
  index (h, t) a = index'' (h, t) a
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = length (range (h, t))

instance Ix Integer where
  range (h, t) = [h..t]
  index (h, t) a = index'' (h, t) a
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = length (range (h, t))

instance Ix Char where
  range (h, t) = [h..t]
  index (h, t) a = index'' (h, t) a
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = length (range (h, t))

instance (Ix a, Ix b) => Ix (a,b) where
  range ((h1, h2), (t1, t2)) = toTuple (range (h1, t1)) (range (h2, t2))
  index (h, t) a = index'' (h, t) a
  inRange ((h1, h2), (t1, t2)) (a, b) = h1 <= a && t1 >= a && h2 <= b && t2 >= b
  rangeSize (h, t) = length (range (h, t))

--data Maybe e = Nothing | Just e

data Tree e = Empty | Node (Tree e) Int e (Tree e)

instance Eq (Tree e) where
	Empty == Empty = True
	Node l1 i1 e1 r1 == Node l2 i2 e2 r2 = l1 == l2 && i1 == i2 && r1 == r2
	_ == _ = False

instance Show a => Show (Tree a) where
	show t = case t of
		Empty -> "E"
		Node left i e right -> "N (i: " ++ show i ++ " e: " ++ show e ++ ")\n\n(" ++ show left ++ ")\n\n(" ++ show right ++ ")"


data Array i e = Array (Tree (Maybe e)) (i, i) deriving (Show)

listArray :: (Ix i) => (i, i) -> [e] -> Array i e
listArray rang list = Array (listTree (rangeSize rang) 0 list) rang

listTree :: Int -> Int -> [e] -> Tree (Maybe e)
listTree size _ [] = Empty --Node Empty (quot size 2) Nothing Empty
listTree 0 _ _ = Empty
listTree size i list = 
	Node
	(listTree half i $ take half list)
	(half+i) (returnE half list)
	(listTree (size-half-1) (half+1+i) $ drop (half+1) list)
  where half = quot size 2



--listTree rang l = listTree' ((rangeSize rang) `quot` 2) 0 l

--listTree' :: Int -> Int -> [e] -> Tree (Maybe e)
--listTree' 0 _ _ = Empty
--listTree' _ _ [] = Empty
--listTree' size i list =
--    Node
--    (listTree' half i $ take half list)
--    (i+half) (returnE half list)
--    (listTree' (half-1) (i+half+1) $ drop (half+1) list)
--    where half = size `quot` 2

returnE :: Int -> [e] -> Maybe e
returnE _ [] = Nothing
returnE n l =
  if (n >= length (take (n+1) l)) then
    Nothing
  else
    Just (l !! n)


--test = listTree' 8 0 ['a']

--main =
--  do putStrLn $ show $ test


(!) :: Ix i => Array i e -> i -> e
(!) (Array tree@(Node left i e right) rang) ind = getElement tree (index rang ind)

fromJust :: Maybe a -> a
fromJust Nothing = error "MyArray: nothing"
fromJust (Just a) = a

getElement :: Tree (Maybe e) -> Int -> e
getElement Empty index = error "MyArray: not initialized"
getElement (Node left i e right) index
  | i == index = fromJust e
  | i > index = getElement left index
  | i < index = getElement right index


elems :: Ix i => Array i e -> [e] 
elems (Array (Empty) rang) = []
elems (Array tree rang) = treeElems tree

treeElems :: Tree (Maybe e) -> [e]
treeElems Empty = []
treeElems (Node left i Nothing right) = treeElems left ++ treeElems right
treeElems (Node left i e right) = treeElems left ++ ((fromJust e) : treeElems right)



insert :: Tree (Maybe e) -> Int -> Int -> Int -> e -> Tree (Maybe e)
insert Empty ind size key el
	| key == (ind + half) = Node Empty key (Just el) Empty
	| key < (ind + half) = Node (insert Empty ind half key el) (ind+half) Nothing Empty
	| key > (ind + half) = Node (insert Empty (half+1+ind) (size-half-1) key el) (ind+half) Nothing Empty
	where half = quot size 2
insert (Node left i e right) ind size key el
	| key == i = Node left i (Just el) right
	| key < i = Node (insert left ind half key el) i e right
	| key > i = Node left i e (insert right (half+1+ind) (size-half-1) key el)
  where half = quot size 2



update :: Ix i => i -> e -> Array i e -> Array i e
update i e (Array tree rang) =
	Array (insert tree 0 (rangeSize rang) (index rang i) e) $ rang

(//) :: (Ix i) => Array i e -> [(i,e)] -> Array i e
(//) array list =
	foldr (\(i, e) arr -> update i e arr) array list

initTree :: (Ix i) => (i,i) -> Tree (Maybe e)
initTree rang = Node Empty (quot (rangeSize rang) 2) Nothing Empty

array :: (Ix i) => (i, i) -> [(i,e)] -> Array i e 
array rang list =
	(//) (Array (initTree rang) rang) list

listArray' :: (Ix i) => (i, i) -> [e] -> Array i e
listArray' rang list =
	array rang (joinLists (range rang) list)

joinLists :: [a] -> [b] -> [(a,b)]
joinLists [] _ = []
joinLists _ [] = []
joinLists (x:xs) (y:ys) = (x,y) : joinLists xs ys 

--toList (Node a b c) = toList b ++ (a : toList c)


--instance (Show e, Show (i e)) => Show (Array i e) where
--  show t = case t of
--    Empty -> "E"
--    Node i e left right -> show i


--(" ++ show left ++ ") (" ++ show right ++ ")"

-- (1p za klasę indeksów i instancje)
{-
typ tablic Array, z operacjami              -- | Buduje tablicę dla danego zakresu i listy elementów
listArray :: (Ix i) => (i, i) -> [e] -> Array i e     -- | Daje element tablicy o podanym indeksie
(!)       :: Ix i => Array i e -> i -> e        -- | Daje listę elementów tablicy (w kolejności indeksów)
elems     :: Ix i => Array i e -> [e]           -- | Buduje tablicę z podanej listy par (indeks,wartość)
array     :: (Ix i) => (i, i) -> [(i,e)] -> Array i e   -- | Daje tablicę będącą wariantem danej, zmienioną pod podanym indeksem
update    :: Ix i => i -> e -> Array i e -> Array i e   -- | Daje tablicę będącą wariantem danej, zmienioną pod podanymi indeksami
(//)  :: (Ix i ) => Array i e -> [(i,e)] -> Array i e


-}
