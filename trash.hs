getElement :: Tree (Maybe e) -> Int -> e
getElement Empty _ = error "Not in scope"
getElement Node left i e right index
	| i == index = e
	| i < index = e

(!) :: Ix i => Array i e -> i -> e
(!) Empty _ _ = error "Not in scope"
(!) tree@(Node left i e right) rang ind = getElement tree (index rang ind)


(!) arr ind = case arr of
 	Empty range -> error "Not in scope"
	Node left i e right ->
		if (ind == i)
			then e
		else
			(!) right ind


let x = listArray (1, 10) ['a'..'g']
