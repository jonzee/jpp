import Data.Set (Set, insert, member, notMember, singleton, toList)
import System.Environment
import System.Directory
import System.IO
import MyArray

type Graph = Array Int [Int]

findMax' :: [[Int]] -> Int -> Int
findMax' [] max = max
findMax' (x:xs) max
  | head x > max = findMax' xs $ head x
  | otherwise = findMax' xs max

toArray :: [[Int]] -> [(Int,[Int])]
toArray list = map (\(x:xs) -> (x,xs)) list

dfs :: Graph -> Set Int -> [Int] -> Set Int
dfs graph set [] = set
dfs graph set (x:xs)
  | member x set = dfs graph set xs
  | notMember x set = dfs graph (dfs graph (insert x set) $ (!) graph x) xs

graphMain :: [[Int]] -> String
graphMain arr =
  let
    max = findMax' arr 0
    graph = array (0, max) $ toArray arr
    set = dfs graph (singleton 1) ((!) graph 1)
    result = show (toList set)
  in result

toInts :: String -> [[Int]]
toInts = map (\line -> map read (words line)::[Int]) . lines

main = do
  file <- getArgs
  if null file then do
    interact $ graphMain . toInts
  else do
    ifExists <- doesFileExist (head file)
    if ifExists then do
      withFile (head file) ReadMode (\handle -> do
        contents <- hGetContents handle
        putStrLn (graphMain (toInts contents)))
    else
      putStrLn "File does not exist!"
