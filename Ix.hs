module Ix (Ix, range, index, inRange, rangeSize) where

class Ord a => Ix a where
  range :: (a, a) -> [a]
  index :: (a, a) -> a -> Int
  inRange :: (a, a) -> a -> Bool
  rangeSize :: (a, a) -> Int

index' :: Ix a => [a] -> a -> Int -> Int
index' [] _ _ = error "MyArray: index out of range"
index' (x:xs) a acc
  | x == a = acc
  | otherwise = index' xs a (acc+1)

toTuple :: [a] -> [b] -> [(a,b)]
toTuple [] _ = []
toTuple (x:xs) l = (map ((,) x) l) ++ (toTuple xs l)

instance Ix Int where
  range (h, t) = [h..t]
  index (h, t) a
    | inRange (h, t) a = (a - h)
    | otherwise = error "MyArray (index): out of range"
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = (t-h+1)

instance Ix Integer where
  range (h, t) = [h..t]
  index (h, t) a
    | inRange (h, t) a = fromInteger (a - h)
    | otherwise = error "MyArray (index): out of range"
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = fromInteger (t-h+1)

instance Ix Char where
  range (h, t) = [h..t]
  index (h, t) a
    | inRange (h, t) a = (fromEnum a) - (fromEnum h)
    | otherwise = error "MyArray (index): out of range"
  inRange (h, t) a = h <= a && t >= a
  rangeSize (h, t) = (fromEnum t) - (fromEnum h) + 1

instance (Ix a, Ix b) => Ix (a,b) where
  range ((h1, h2), (t1, t2)) = toTuple (range (h1, t1)) (range (h2, t2))
  --index ((h1, h2), (t1, t2)) (h, t)
  --  | inRange ((h1, h2), (t1, t2)) (h, t) = index (h1,) * (t - h2)
  --  | otherwise = error "MyArray (index): out of range"  
  inRange ((h1, h2), (t1, t2)) (a, b) = h1 <= a && t1 >= a && h2 <= b && t2 >= b
  rangeSize ((h1, h2), (t1, t2)) = rangeSize(h1, t1) * rangeSize(h2, t2)